Scurta descriere. Ce ati scrie despre aplicatie daca ati publica-o pe Google Play Store?
- Walkthrough
- Functionalitati

Aveți cele mai recente informații despre COVID-19 din toata lumea. Această aplicație afișează cele
mai recente actualizări  despre focare.
Obiectivul aplicatiei este de a tine populatia informata despre evolutia bolii COVID-19.

Aplicatia dispune de o interfata simplista si usor de inteles de utilizatori. In ecranul principal,
utilizatorul trebuie sa se conecteze cu contul de Google pentru a putea trece mai departe. Urmatorul
ecran contine fiecare tara cu detalii despre numarul total de cazuri din acea tara, numarul persoanelor
care au inca boala, numarul persoanelor vindecate si numarul de morti. In lista cu tarile exista si
optiunea de search pentru a face cautarea mai rapida in functie de tara dorita. In meniu este optiunea
"Capture" unde se poate fotografia o persoana care nu poarta masca. Tot in meniu se gaseste optiunea "Share"
cu ajutorul careia se poate trimite un mesaj/email cu datele din intreaga lume.

Aplicatia are urmatoarele functionalitati:
    - conectare cu cont Google
    - share de informatii intre aplicatie si restul aplicatiilor
    - accesarea camerei pentru fotografii
    - vizionarea listei cu numarul de cazuri

Link video : https://bit.ly/3eNSx1W

Cerinte:
    Implementat o operatie cu camera (facut poze sau filmat)
    Implementat un Recycler View cu functie de cautare
    Utilizat o metoda de navigatie: Navigation Drawer sau Bottom Navigation
    Implementat o metoda de Share (Android Sharesheet)
    Implementat o animatie folosind ObjectAnimator si una folosind MotionLayout
    Social Login (Google)
    UI adaptat pentru landscape mode
    Web services (Retrofit, Moshi, Firebase)
    Dependency injection (Koin)
