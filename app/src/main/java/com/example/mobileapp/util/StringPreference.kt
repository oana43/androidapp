package com.example.mobileapp.util

import android.content.SharedPreferences

class StringPreference(
    private val sharedPreferences: SharedPreferences,
    private val key: String,
    private val defaultValue: String? = null
) {

    fun getValue(): String? =
        sharedPreferences.getString(key, defaultValue)

    fun setValue(value: String?) =
        sharedPreferences.edit()
            .putString(key, defaultValue)
            .apply()
}