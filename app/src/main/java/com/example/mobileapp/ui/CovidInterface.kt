package com.example.mobileapp.ui

import com.example.mobileapp.model.CovidCountry
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET

interface CovidInterface {
    @GET("countries")
    fun getCountries(): Deferred<Response<List<CovidCountry>>>
}