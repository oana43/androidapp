package com.example.mobileapp.ui

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.mobileapp.R
import com.example.mobileapp.databinding.FragmentPictureBinding
import kotlinx.android.synthetic.main.fragment_picture.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


class PictureFragment : Fragment() {

    private lateinit var binding: FragmentPictureBinding
    private var currentPhotoPath : String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_picture, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        btPhoto.setOnClickListener {
            if (checkCameraPermission()){
                dispatchTakePictureIntent()
            }

        }
    }

    private fun checkCameraPermission(): Boolean {
        val hasCameraPermission = ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
        val hasWriteStoragePermission = ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
        val hasReadStoragePermission = ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
        if (!hasCameraPermission || !hasWriteStoragePermission || !hasReadStoragePermission) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    requireActivity(),
                    Manifest.permission.CAMERA
                ) ||
                ActivityCompat.shouldShowRequestPermissionRationale(
                    requireActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )||
                ActivityCompat.shouldShowRequestPermissionRationale(
                    requireActivity(),
                    Manifest.permission.READ_EXTERNAL_STORAGE
                )
            ) {
                val builder = AlertDialog.Builder(requireContext())
                builder.apply {
                    setPositiveButton(
                       "OK"
                    ) { _, _ ->
                        askForPermissions()
                    }
                    setNegativeButton(
                        "Cancel"
                    ) { _, _ ->
                        // User cancelled the dialog
                    }

                    setMessage("In order to take a picture and display it you need to provide camera and storage access.")
                    setTitle("Alert")
                }

                val alertDialog: AlertDialog? = builder.create()
                alertDialog?.show()
            } else {
                askForPermissions()
            }
            return false
        }else{
            return true
        }
    }

    private fun askForPermissions() {
        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE),
            CAMERA_STORAGE_RC
        )
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(requireActivity().packageManager)?.also {
                val file = createImageFile()
                file.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        requireContext(),
                        "com.example.mobileapp.fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, CAMERA_STORAGE_RC)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        currentPhotoPath?.let{
            setPic()
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            CAMERA_STORAGE_RC -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(requireContext(), "Permissions granted", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(requireContext(), "Permissions not granted", Toast.LENGTH_SHORT).show()
                }
                return
            }
            else -> {
                // Ignore all other requests.
            }
        }
    }

    private fun createImageFile(): File {
        // Create an image file name
        val storageDir: File? = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val f = File(storageDir, PICTURE_FILE_NAME)
        f.createNewFile()
        currentPhotoPath = f.absolutePath
        return f
    }

    private fun setPic() {
        // Get the dimensions of the View
        val targetW = ivAvatar.width
        val targetH = ivAvatar.height

        val bmOptions = BitmapFactory.Options().apply {
            // Get the dimensions of the bitmap
            inJustDecodeBounds = true

            val photoW: Int = outWidth
            val photoH: Int = outHeight

            // Determine how much to scale down the image
            val scaleFactor: Int = Math.min(photoW / targetW, photoH / targetH)

            // Decode the image file into a Bitmap sized to fill the View
            inJustDecodeBounds = false
            inSampleSize = scaleFactor
        }
        BitmapFactory.decodeFile(currentPhotoPath, bmOptions)?.also { bitmap ->
            ivAvatar.setImageBitmap(bitmap)
        }
    }

    companion object{
        const val CAMERA_STORAGE_RC =1
        const val PICTURE_FILE_NAME = "avatar.png"
    }

}
