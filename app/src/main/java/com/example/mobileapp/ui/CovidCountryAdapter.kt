package ro.carrefour.wmsnative.ui.checkarticle

import android.view.LayoutInflater
import android.view.ViewGroup

import androidx.recyclerview.widget.RecyclerView
import com.example.mobileapp.databinding.ItemCountryBinding
import com.example.mobileapp.model.CovidCountry

class
CovidCountryAdapter : RecyclerView.Adapter<CovidCountryViewHolder>() {

    private var data = ArrayList<CovidCountry>()

    fun setData(data: List<CovidCountry>?) {
        this.data = ArrayList()
        if (data != null) {
            this.data.addAll(data)
        }
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CovidCountryViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemCountryBinding.inflate(layoutInflater, parent, false)

        return CovidCountryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CovidCountryViewHolder, position: Int) {
        val currentLocation = data[position]
        holder.bind(currentLocation)
    }

}

class CovidCountryViewHolder(val binding: ItemCountryBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(item: CovidCountry) {
        binding.entry = item
        if(item.active != null){
            binding.tvActive.text = "Active:  ${item.active?.toString()}"
        }
        else{
            binding.tvActive.text = "Active:  0"
        }
        if(item.cases != null){
            binding.tvCases.text = "Cases:  ${item.cases?.toString()}"
        }
        else{
            binding.tvCases.text = "Cases:  0"
        }

        if(item.deaths != null){
            binding.tvDeaths.text = "Deaths:  ${item.deaths?.toString()}"
        }
        else{
            binding.tvDeaths.text = "Deaths:  0"
        }

        if(item.recovered != null){
            binding.tvRecovered.text = "Recovered:  ${item.recovered?.toString()}"
        }
        else{
            binding.tvRecovered.text = "Recovered:  0"
        }
    }
}

