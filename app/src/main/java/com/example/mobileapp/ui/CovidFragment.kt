package com.example.mobileapp.ui

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.example.mobileapp.R
import com.example.mobileapp.databinding.FragmentCovidBinding
import com.example.mobileapp.model.CovidCountry
import kotlinx.coroutines.*
import org.koin.android.ext.android.inject
import ro.carrefour.wmsnative.ui.checkarticle.CovidCountryAdapter
import kotlin.collections.ArrayList

class CovidFragment : Fragment() {
    private val adapter = CovidCountryAdapter()
    private var result: List<CovidCountry> = ArrayList()
    val serviceBuilderAPI: ServiceBuilder by inject()

    private lateinit var binding: FragmentCovidBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_covid, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rvLocations.adapter = adapter
        binding.etSearch.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                val temp = ArrayList(result)
                for (t in result){
                    if (!t.country?.toLowerCase().contains(s.toString().toLowerCase())){
                        temp.remove(t)
                    }
                }
                adapter.setData(temp)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })

        getCovidData()

    }

    private fun getCovidData() {
        val covidInterface = serviceBuilderAPI.getCovidInterface()

        lifecycleScope.launch(Dispatchers.Main) {
            result = getCovidCountries(covidInterface)
            adapter.setData(result)
        }


    }

    private suspend fun getCovidCountries(covidInterface: CovidInterface): ArrayList<CovidCountry> {
        return withContext(Dispatchers.IO) {
            val temp: ArrayList<CovidCountry> = arrayListOf()
            val request = covidInterface.getCountries()
            try {
                val response = request.await()
                if (response.isSuccessful) {

                    val data: List<CovidCountry>? = response.body()

                    data?.let {
                        temp.addAll(data)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            temp
        }

    }


}
