package com.example.mobileapp.ui

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.core.view.GravityCompat
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.preference.PreferenceManager
import com.example.mobileapp.R
import com.example.mobileapp.model.CovidCountry
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.header.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.android.ext.android.inject

class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var navController : NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        navController = findNavController(R.id.nav_host_fragment)
        nvMain?.setNavigationItemSelectedListener(this)

        val sharedPreference = PreferenceManager.getDefaultSharedPreferences(this)
        val accountName = sharedPreference.getString("account", "")
        nvMain.getHeaderView(0).tvHeader.text = accountName

    }

    private fun signOut() {
        startActivity(
            MainActivity.getLaunchIntent(
                this
            )
        )
        FirebaseAuth.getInstance().signOut()
    }

    companion object {
        fun getLaunchIntent(from: Context) = Intent(from, HomeActivity::class.java)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.actionCovid-> {
                navController.navigate(R.id.toCovidFragment)
            }
            R.id.actionShare->{
                getCovidData()
            }
            R.id.actionLogout->{
                signOut()
            }
            R.id.actionPicture->{
                navController.navigate(R.id.toPictureFragment)
            }
            else -> return false
        }

        dl_main.closeDrawer(GravityCompat.START)

        return true
    }

    private fun share(text: String){
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, "COVID-19 stats:\n$text")
            type = "text/plain"
        }

        val shareIntent = Intent.createChooser(sendIntent, null)
        startActivity(shareIntent)
    }

    private fun getCovidData() {
        //Injected GithubServiceAPI
        val serviceBuilderAPI: ServiceBuilder by inject()

        val covidInterface = serviceBuilderAPI.getCovidInterface()

        lifecycleScope.launch(Dispatchers.Main) {
            val result = getCovidCountries(covidInterface)
            share("Cases: ${result[0].cases}, Active: ${result[0].active}, Recovered: ${result[0].recovered}, Deaths: ${result[0].deaths}")
        }


    }

    private suspend fun getCovidCountries(covidInterface: CovidInterface): ArrayList<CovidCountry> {
        return withContext(Dispatchers.IO) {
            val temp: ArrayList<CovidCountry> = arrayListOf()
            val request = covidInterface.getCountries()
            try {
                val response = request.await()
                if (response.isSuccessful) {

                    val data: List<CovidCountry>? = response.body()

                    data?.let {
                        temp.addAll(data)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            temp
        }

    }


}