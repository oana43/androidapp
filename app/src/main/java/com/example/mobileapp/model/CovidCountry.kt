package com.example.mobileapp.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CovidCountry(
    @field:Json(name = "country") @Json(name = "country")
    var country: String,

    @field:Json(name = "cases") @Json(name = "cases")
    var cases: Int? = 0,

    @field:Json(name = "deaths") @Json(name = "deaths")
    var deaths: Int? = 0,

    @field:Json(name = "recovered") @Json(name = "recovered")
    var recovered: Int? = 0,

    @field:Json(name = "active") @Json(name = "active")
    var active: Int? = 0

) : Parcelable