package com.example.mobileapp

import android.app.Application
import com.example.mobileapp.ui.ServiceBuilder
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.dsl.module

class CovidApp : Application() {


    var listofModules = module {
        single { ServiceBuilder() }
    }


    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@CovidApp)
            modules(listofModules)
        }
    }
}
